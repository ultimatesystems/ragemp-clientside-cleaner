﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clientside_cleaner.Services
{
    public class Logger
    {
        private static void PrintPrefix(ConsoleColor TypeColor = ConsoleColor.White, string Type = null)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("[");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write("UltimateSystems");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("] ");

            if(Type != null)
            {
                Console.ForegroundColor = TypeColor;
                Console.Write("[");
                Console.Write(Type);
                Console.Write("] ");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public static void Log(string Text)
        {
            PrintPrefix();
            Console.Write(Text + "\n");
        }

        public static void LogError(string text)
        {
            PrintPrefix(ConsoleColor.Red, "ERROR");
            Console.Write(text + "\n");
        }
    }
}
