﻿using clientside_cleaner.Services;
using System;
using Microsoft.Win32;
using System.IO;

namespace clientside_cleaner
{
    class Program
    {
        static void Main(string[] args)
        {
            var rageMp = "HKEY_CURRENT_USER" + "\\" + "Software" + "\\" + "RAGE-MP";
            var rageMpPath = Registry.GetValue(rageMp, "rage_path", null);
            if(rageMpPath == null)
            {
                Logger.LogError("We cant find RageMP on your Computer. Please install or reinstall RageMP and start the Clientside cleaner again.");
                Console.ReadLine();
                return;
            }

            Logger.Log("RageMP Path: " + rageMpPath);
            Logger.Log("Is this your RageMP Path ?");

            Logger.Log("Please answer with yes or no.");

            var yesOrNo = Console.ReadLine();
            if(yesOrNo != "yes")
            {
                Logger.Log("Please enter your RageMP Path");
                Logger.Log(@"Example: C:\RageMP");

                var path = Console.ReadLine();
                Registry.SetValue(rageMp, "rage_path", path);

                Logger.Log("Okay. Please start the Clientside cleaner again.");
                Console.ReadLine();
                return;
            }

            Logger.Log("Welcome. Please enter a server IP from which we should delete the client resources.");

            var serverIP = Console.ReadLine();
            var resourcesPath = rageMpPath + "\\" + "client_resources" + "\\" + serverIP.Replace(":", "_");

            if (Directory.Exists(resourcesPath))
            {
                try
                {
                    Directory.Delete(resourcesPath, true);

                    Logger.Log("The Client resources deleted now!");
                    Logger.Log("If you have Feedback or Bug reports please Contact us on our Discord.");
                    Logger.Log("Goodbye!");

                    Console.ReadLine();
                    return;
                } catch(Exception ex)
                {
                    Logger.LogError("Owww... We cant delete the Folder.");
                    Logger.LogError("Please report this to a Moderator of UltimateSystems.");
                    Logger.LogError("ERROR: " + ex.Message);
                    Console.ReadLine();
                    return;
                }
            } else {
                Logger.Log("We cant find Client Resources for that ServerIP.");
                Logger.Log("Please restart and try again.");
                Console.ReadLine();
                return;
            }
        }
    }
}
